# 控制台

> 简单的控制台

## 静态方法

> 注意: `Log`, `Write`, `WriteLine` 只是方便调用的实现, 他们的打印级别都是trace

### Log - 打印信息

- static void Write(params object[] items)
- `items` 可变数组, 当数组长度大于1时, 使用空格拼接

```C#
Console.Write("hello astator");
```



### Write - 打印信息
- static void Write(params object[] items)

```C#
Console.Write("hello astator");
```


## WriteLine - 打印信息

- static void WriteLine(params object[] items)

```c#
Console.WriteLine("hello astator");
```



### Trace - 打印信息, trace级别

- static void Trace(params object[] items)

```c#
Console.Trace("hello astator");
```



### Debug- 打印信息, debug级别

- static void Debug(params object[] items)

```c#
Console.Debug("hello astator");
```



### Info - 打印信息, Info级别

- static void Info(params object[] items)

```c#
Console.Info("hello astator");
```



### Wran - 打印信息, wran级别

- static void Wran(params object[] items)

```c#
Console.Wran("hello astator");
```



### Error - 打印信息, error级别

- static void Error(params object[] items)

```c#
Console.Error("hello astator");
```



### Fatal - 打印信息, fatal级别

- static void Fatal(params object[] items)

```c#
Console.Fatal("hello astator");
```



### ShowAsync - 启动一个控制台悬浮窗 

- static async Task ShowAsync(string title = "控制台", int width = 300, int height = 500, int x = 0, int y = 0, GravityFlags gravity = GravityFlags.Left | GravityFlags.Top, WindowManagerFlags flags = WindowManagerFlags.LayoutNoLimits | WindowManagerFlags.NotTouchModal)
- `title` 标题
- `width` 宽度
- `height` 高度
- `x`  起始位置x
- `y`  起始位置y
- `gravity` 重力
- `flags ` 悬浮窗的标志, 一般情况无需改动 

需要悬浮窗权限, 当调用此方法前已经有一个悬浮窗时, 会关闭上一个悬浮窗

```C#
await Console.ShowAsync();
await Console.ShowAsync("我的控制台", 350,300);
```



### Close - 关闭控制台悬浮窗

- static void Close()

```C#
Console.Close();
```



### Hide - 隐藏控制台悬浮窗

static void Hide()

```c#
Console.Hide();
```



### Clear - 清空控制台悬浮窗内容

static void Clear()

```c#
Console.Clear();
```



### SetTitle - 设置控制台标题

static void SetTitle(string title)

```c#
Console.SetTitle("console");
```



### ReadInputAsync - 读取用户输入

- static async Task<`string`> ReadInputAsync(int timeout)
- `timeout` 超时时间, 超时返回空字符串

```c#
var input = await Console.ReadInputAsync(60000);
```



### SetLogLevelColors - 设置不同级别的打印颜色

- static void SetLogLevelColors(string trace = "#4a4a4d", string debug = "#4a4a4d", string info = "#4a4a4d", string wran = "#f0dc0c", string error = "red", string fatal = "red")
- `trace `  trace级别的打印颜色
- `debug` debug级别的打印颜色
- `info` info级别的打印颜色
- `wran` debug级别的打印颜色
- `error` debug级别的打印颜色
- `fatal` fatal级别的打印颜色

```c#
Console.SetLogLevelColors(trace: "#422517", debug: "#a76283", info: "#107c10", wran: "#b4884d", error: "#c12c1f", Fatal: "#e60012");
```
