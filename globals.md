# Globals - 全局方法

## 静态属性

### AppContext

- static Context AppContext { get; set; }



## 静态方法

### Toast

- static void Toast(object text, ToastLength duration = ToastLength.Short)
- `text` 要展示的消息
- `duration` 持续时间, 默认`ToastLength.Short`

给用户展示简短消息的视图
```c#
Globals.Toast("toast");
```



### InvokeOnMainThreadAsync - 在ui线程执行action

- static Task InvokeOnMainThreadAsync(Action action)
- static `Task<T>` InvokeOnMainThreadAsync`<T>`(`Func<T>` func)
- static Task InvokeOnMainThreadAsync(`Func<Task>` funcTask)
- `Task<T>` InvokeOnMainThreadAsync`<T>`(`Func<Task<T>>` funcTask)

```c#
await = Globals.InvokeOnMainThreadAsync(() =>
{
	Globals.Toast(Thread.CurrentThread.ManagedThreadId.ToString());
});
```



### GetCurrentPackageName - 通过使用情况访问权限获取前台应用包名

- static string GetCurrentPackageName()
- 当没有使用情况访问权限时返回null

```c#
var pkgName = Globals.GetCurrentPackageName();
```



### LaunchApp - 启动其他应用

- static bool LaunchApp(string pkgName)
- `pkgName`  包名

```c#
Globals.LaunchApp("com.tencent.mobileqq");
```



### GetInstalledPackages - 获取已安装应用的信息

- static IList<Android.Content.PM.PackageInfo> GetInstalledPackages()

```c#
var list = Globals.GetInstalledPackages();
```



### InstallApp - 安装应用

- static void InstallApp(string path)
- `path`  文件路径

```c#
Globals.InstallApp("/sdcard/myapp.apk");
```

