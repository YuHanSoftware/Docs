# 引用dll

astator引用dll和普通c#项目引用dll没有区别。

比如我们要引用一个`test.dll`, 先把这个dll复制到ref文件夹里, 然后打开`*.csproj`文件, 在空白处填入:

```xml
<ItemGroup>
	<Reference Include="test">
		<HintPath>./ref/test.dll</HintPath>
	</Reference>
</ItemGroup>
```
