# AnchorGraphicHelper - 锚点找色比色

适用于 16+:9 比例分辨率的手游脚本多分辨率适配

建议在720x1280p分辨率配合图色助手取色

图色助手: https://gitee.com/yiszza/ScriptGraphicHelper/releases

不保证适用于所有游戏, 使用前请测试

取点时先观察控件的布局, 可能是居中, 或者是靠左或者是靠右, 然后进行取色

## 多分辨率适配原理简单解析
明日方舟720x1280与720x1560分辨率下的对比

![](assets/anchor_desc.png)



## 静态方法

### Create - 创建对象

- static AnchorGraphicHelper Create(int devWidth, int devHeight, int left, int top, int right, int bottom)
- `devWidth` 开发分辨率宽度
- `devHeight` 开发分辨率高度
- `left` 当前游戏显示范围左上 x 坐标
- `top` 当前游戏显示范围左上 y 坐标
- `right` 当前游戏显示范围右下 x 坐标
- `bottom` 当前游戏显示范围右下 y 坐标

```c#
var helper = AnchorGraphicHelper.Create(1280, 720, 0, 0, 1279, 719);
```



## 实例方法

<!--### ReInitialize - 重新初始化-->

### KeepScreen - 获取截图数据

- bool KeepScreen(bool sign)
- `sign` 是否需要调用多点找色

```c#
if (helper.KeepScreen())
{
    Globals.Toast("获取截图数据成功");
}
```



### UpdateRedList - 更新r值映射集合, 用于多点找色

- void UpdateRedList()

```c#
helper.UpdateRedList();
```



### GetBounds - 获取与运行分辨率相关的范围

- Rect GetBounds(int left, int top, int right, int bottom, params AnchorMode[] mode)
- `left` 开发分辨率范围: 左上 x 坐标
- `top` 开发分辨率范围: 左上 y 坐标
- `right` 开发分辨率范围: 右下 x 坐标
- `bottom` 开发分辨率范围: 右下 y 坐标
- `mode` 锚点对齐方式

```c#
var bounds = helper.GetBounds(0, 0, 200, 200, AnchorMode.Center);
var bounds = helper.GetBounds(0, 0, 200, 200, AnchorMode.Left, AnchorMode.Right);
```



### GetPoint - 获取与运行分辨率相关的坐标

- Point GetPoint(int x, int y, AnchorMode mode)
- `x`: 开发分辨率的x坐标
- `y` 开发分辨率的y坐标
- `mode` 锚点对齐方式

```c#
var point = helper.GetPoint(100, 100, AnchorMode.Center);
```



### GetPixel - 获取与运行分辨率相关的指定像素数据

- `int[]` GetPixel(int x, int y, AnchorMode mode)
- string GetPixelString(int x, int y, AnchorMode mode)
- int GetPixelHex(int x, int y, AnchorMode mode)
- `x`: 开发分辨率的 x 坐标
- `y` 开发分辨率的 y 坐标
- `mode` 锚点对齐方式

```c#
var rgb = helper.GetPixel(100, 100, AnchorMode.Left);
var hexStr = helper.GetPixelString(100, 100, AnchorMode.Center);
var hexInt = helper.GetPixelHex(100, 100, AnchorMode.Right);
```



### ParseCmpColorString - 解析与运行分辨率相关的比色色组描述

- `int[][]` ParseCmpColorString(string description)
- `description` 色组字符串

```c#
var desc = helper.ParseCmpColorString("176|149|0x263238,110|340|0x37474f,106|514|0xfafafa");
```



### ParseFindColorString - 解析与运行分辨率相关的找色色组描述

- `int[][]` ParseFindColorString(string description)
- `description` 色组字符串

```c#
var desc = helper.ParseFindColorString("176|149|0x263238,110|340|0x37474f,106|514|0xfafafa");
```



### CompareMultiColor - 多点比色

- bool CompareMultiColor(`int[][]` description, int sim, bool isOffset)
- `description` 色组数组
- `sim` 相似度, 范围0~100
- `isOffset` 是否偏移查找

```c#
var result = helper.CompareMultiColor(desc, 95, true);
```



### CompareMultiColorLoop - 条件循环多点比色

- bool CompareMultiColorLoop(`int[][]` description, int sim, bool isOffset, int timeout, int timelag, bool sign)
- `timeout` 超时时间, 单位为毫秒
- `timelag` 间隔时间, 单位为毫秒
- `sign` 跳出条件, true为比色成功时返回, false为比色失败时返回

```c#
var result = helper.CompareMultiColorLoop(desc, 95, true, 5000, 500, true)
```



### FindMultiColor - 多点找色

- Point FindMultiColor(int left, int top, int right, int bottom, `int[][]` description, int sim, bool isOffset)
- Point FindMultiColor(Rect bounds, `int[][]` description, int sim, bool isOffset)
- `left` 查找范围: 左上 x 坐标
- `top` 查找范围: 左上 y 坐标
- `right` 查找范围: 右下 x 坐标
- `bottom` 查找范围: 右下 y 坐标
- `bounds` 查找范围
- `description` 色组描述
- `sim` 相似度, 0~100
- `isOffset` 是否偏移查找

```c#
var point = helper.FindMultiColor(0, 0, 1000, 1000, desc, 95, true);
var bounds = new Rect(0, 0, 1000, 1000);
var point = helper.FindMultiColor(bounds, desc, 95, true);
```



### FindMultiColorEx - 多点找色并返回所有坐标

- `List<Point>` FindMultiColorEx(int left, int top, int right, int bottom, `int[][]` description, int sim, int filterNum = -1)
- `List<Point>` FindMultiColorEx(Rect bounds, `int[][]` description, int sim, int filterNum = -1)
- `filterNum` 过滤半径, 默认-1, 去除重叠区域

```
var points = helper.FindMultiColorEx(0, 0, 1000, 1000, desc, 95, -1);
var bounds = new Rect(0, 0, 1000, 1000);
var points = helper.FindMultiColorEx(bounds, desc, 95, -1);
```



### FindMultiColorLoop - 条件循环多点找色

- Point FindMultiColorLoop(int left, int top, int right, int bottom, `int[][]` description, int sim, bool isOffset, int timeout, int timelag, bool sign)
- Point FindMultiColorLoop(Rect bounds, `int[][]` description, int sim, bool isOffset, int timeout, int timelag, bool sign)
- `timeout` 超时时间, 单位为毫秒
- `timelag` 间隔时间, 单位为毫秒
- `sign` 跳出条件, true为找色成功时返回, false为找色失败时返回

```c#
var point = helper.FindMultiColorLoop(0, 0, 1000, 1000, desc, 95, true, 5000, 500, true);
var bounds = new Rect(0, 0, 1000, 1000);
var point = helper.FindMultiColor(bounds, desc, 95, true, 5000, 500, true);
```
