# 引用nuget包

astator引用nuget包和普通c#项目引用nuget包没有区别。



## 在Nuget包管理器中查找引用

鼠标项目的`依赖项`, 选择`管理nuget程序包`, 在管理器页面点击`浏览`, 输入包名进行安装



## 在csproj文件里引用

比如我们要引用`Serilog`, 指定`2.10.0`版本, 打开`*.csproj`文件, 在空白处填入:

```xml
<ItemGroup>
	<PackageReference Include="Serilog" Version="2.10.0" />
</ItemGroup>
```

如果要自动引用最新版本, 修改Version为`*`:

```xml
<ItemGroup>
	<PackageReference Include="Serilog" Version="*" />
</ItemGroup>
```



