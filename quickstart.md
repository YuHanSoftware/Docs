# 快速入门

## 运行环境安装

1. 安装[vs 2022预览版](https://visualstudio.microsoft.com/zh-hans/vs/preview/#download-preview)
2. 打开Visual studio Installer, 点击修改, 勾选`使用.NET的移动开发`, 然后点击右下角的修改
3. 等待安装完成




## 使用VS 2022预览版开发
1. 打开VS 2022预览版, 选择`继续但无需代码`进入IDE界面
2. 选择菜单栏的`扩展` -> `管理扩展`进入扩展管理界面
3. 选择`联机`, 在搜索框搜索`astator-ext`, 搜索完成后选择下载安装
4. 新建项目, 找到`astator项目`创建, 新建完成后选择菜单栏的`视图` -> `其他窗口` -> `astator调试窗口`, 然后你会在屏幕下方看到astator的调试窗口




## 使用VS Code开发

电脑需安装[VS Code](https://code.visualstudio.com/)

打开VS Code扩展商店, 搜索`C#`安装。

打开VS Code扩展商店, 搜索`astator-ext`安装。

打开VS Code设置, 搜索`omnisharp.enableImportCompletion`, 勾选它



## 项目结构

首先打开VS 2022创建一个astator项目, 我们可以看到, 脚本项目结构如下:

```
*
└── assets
├── ref
├── Core
	├──GlobalUsings.cs
	└──Runtime.cs
├── Program.cs
└──  *.csproj
```

`Program.cs`  入口类文件。

`*.csproj`  项目配置文件。

`GlobalUsings.cs`  全局using声明文件。

`Runtime.cs`  对`ScriptRuntime`类的封装, 方便调用。

`assets`为存放脚本资源的文件夹, 当打包apk时, 将该文件夹的所有文件打包到apk。

`ref`为存放引用dll文件夹, 当打包apk时, 将该文件夹下的dll文件打包到apk。

除以上两个文件夹外, 当打包apk时, 你在其他文件夹下的所有非.cs文件都将在打包apk时被忽略。



## 入口方法

我们把`Program.cs`内容修改为以下代码。


```c#
using System;
using astator.Core.Script;

namespace Examples;
public class HelloWorld
{
    [ProjectEntryMethod]
    public static void ProjectMain(ScriptRuntime runtime)
    {
        Console.WriteLine("hello world 1");
        Globals.Toast("hello world 1");
    }

    [ScriptEntryMethod(FileName = "Program.cs")]
    public static void ScriptMain(ScriptRuntime runtime)
    {
        Console.WriteLine("hello world 2");
        Globals.Toast("hello world 2");
    }
}
```

当你执行`运行项目`命令时, 输出`hello world 1`, 当你对`Program.cs`执行`运行脚本`命令时, 输出`hello world 2`。

为什么`运行项目`和`运行脚本`会执行不同的方法呢? 让我们来了解一下astator的两种入口方法特性。



### ProjectEntryMethod - 项目入口方法特性

放置在一个静态公开方法上, 使该方法成为运行项目时的入口方法, 一个项目必须有一个且仅有一个`ProjectEntryMethod`, 当项目拥有一个以上`ProjectEntryMethod`时, 执行第一个找到的`ProjectEntryMethod`。

#### IsUIMode - 是否为ui模式

- bool IsUIMode { get; set; } = false;



### ScriptEntryMethod - 脚本入口方法特性

放置在一个静态公开方法上, 使该方法成为运行脚本(命中)时的入口方法, 一个项目可以拥有多个`ScriptEntryMethod`, 但一个文件仅能拥有一个`ScriptEntryMethod`, 当一个文件拥有一个以上`ScriptEntryMethod `时, 执行第一个找到的`ScriptEntryMethod `。

#### IsUIMode - 是否为ui模式

- bool IsUIMode { get; set; } = false;

#### FileName - 脚本文件名

- string FileName { get; set; } = string.Empty;
