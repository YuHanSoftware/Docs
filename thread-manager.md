# ThreadManager - 线程管理

在astator中, 脚本必须经过此api来实现线程创建, 否则astator无法在脚本停止时卸载相关程序集!

## 实例方法

### Start - 执行

- Thread Start(Action action)
- `return` 创建的线程

以线程方式执行一个action

```c#
var thread = Runtime.Threads.Start(()=>
{
    //执行代码
});
```





### IsAlive - 是否有线程存活

- bool IsAlive()

```c#
var isAlive = Runtime.Threads.IsAlive();
```





### IsLastAlive - 是否只有一个线程存活

- bool IsLastAlive()

```c#
var isLastAlive = Runtime.Threads.IsLastAlive();
```





### Interrupt - 向所有线程发送中断信号

- void Interrupt()

```c#
Runtime.Threads.Interrupt();
```





