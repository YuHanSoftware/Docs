# UIManager - 界面管理

## 实例字段

### this - 索引器

- IView this[string key]

控件索引器

```c#
Runtime.Ui.Create("btn",new ViewAttrs
{
    ["id"] = "test",
});
var btn = Runtime.Ui["test"];
```



## 实例方法

### Alert - 弹窗

- void Alert(string text, string title = "")

- `text` 弹窗消息
- `title` 弹窗标题

弹窗, 当astator具有悬浮窗权限时, 可在任何界面弹出

```c#
Runtime.Ui.Alert("hi");
```



### SetStatusBarColor - 设置状态栏背景颜色

- void SetStatusBarColor(string color)

```c#
Runtime.Ui.SetStatusBarColor("#0000ff");
```





### SetLightStatusBar - 设置状态栏深色字体和图标

- void SetLightStatusBar()

```c#
Runtime.Ui.SetLightStatusBar();
```





### OnResumeListener - 设置脚本activity OnResume回调

- void OnResumeListener(Action callback)

```c#
Runtime.Ui.OnResumeListener(()=>
{
    Globals.Toast("OnResume");
});
```





### OnKeyDownListener - 设置脚本activity OnKeyDown回调

- void OnKeyDownListener(Func<Keycode, KeyEvent, bool> callback)
- `Keycode` 键值
- `KeyEvent` 事件
- `bool` func返回值,  返回true以防止该事件被进一步传播，返回false表示你没有处理此事件

当一个键被按下并且没有被活动内部的任何视图处理时被调用

```c#
Runtime.Ui.OnKeyDownListener((keycode, keyEvent)=>
{
    Globals.Toast("OnKeyDown");
    return false;
});
```





### On - 添加全局控件监听器, 仅在创建view之前添加有效

- void On(string type, string key, object listener)

- `type` 控件类型
- `key` 监听器类型
- `listener` 监听器实例

```c#
Runtime.Ui.On("check", "created", new OnCreatedListener((v) =>
{
	var cb = v as ScriptCheckBox;
	cb.Checked = Preferences.Get<bool>(cb.CustomId, false);
}));

Runtime.Ui.On("spinner", "created", new OnCreatedListener((v) =>
{
	var spinner = v as ScriptSpinner;
	spinner.SetAttr("position", Preferences.Get<int>(spinner.CustomId, 0));
}));
```



### ParseXml - 解析xml

- View ParseXml(string xml)

```c#
var xml = @"
<linear orientation='vertical'>
	<text text='文本控件' textSize='18' textStyle='bold'/>
</linear>
";

var view = Runtime.Ui.PaseXml(xml);
Runtime.Ui.Show(view);
```





### Show - 展示布局

- void Show(View layout)



### Create - 创建控件

- View Create(string type, ViewAttrs attrs)
- `type` 控件类型
- `attrs` 控件参数

```c#
Runtime.Ui.Create("btn", new ViewAttrs
{
    ["id"] = "button"
});

Runtime.Ui.Create("text", new ViewAttrs
{
    ["id"] = "textView"
});
```





### <!--CreateFrameLayout - 创建帧布局控件-->

<!--ScriptFrameLayout CreateFrameLayout(ViewAttrs attrs = null)--> 



