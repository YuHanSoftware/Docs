![](assets/icon.png ' :size=64')
# astator

> astator是使用c#作为脚本的安卓自动化软件

> 支持andorid 7.0 ~ android 12

[GitHub](https://github.com/astator-community/astator)
[码云](https://gitee.com/astator/astator)
[阅读文档](README)
