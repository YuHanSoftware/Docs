# TaskManager - 任务管理

在astator中, 脚本必须经过此api来实现任务创建, 否则astator无法在脚本停止时卸载相关程序集!



## 实例方法

### Run - 执行

- Task Run(`Action<CancellationToken>` action)
- Task Run(`Action<CancellationToken>` action, CancellationToken token)
- Task Run(`Func<CancellationToken, Task>` action)
- Task Run(Func<CancellationToken, Task> action, CancellationToken token)
- `Task<TResult>` Run`<TResult>`(`Func<CancellationToken, TResult>` func)
- `Task<TResult>` Run`<TResult>`(`Func<CancellationToken, TResult>` func, CancellationToken token)
- `Task<TResult>` Run`<TResult>`(`Func<CancellationToken, Task<TResult>>` func)
- `Task<TResult>` Run`<TResult>`(`Func<CancellationToken, Task<TResult>>` func, CancellationToken token)

以任务方式执行一个action或func

```c#
await Runtime.Tasks.Run((token)=>
{
    //执行代码
});

var result = await Runtime.Tasks.Run<string>((token)=>
{
    //执行代码
    return "astator";
});

var source = new CancellationTokenSource();
var result = await Runtime.Tasks.Run((token)=>
{
    //执行代码
}, source.Token);
```





### GetTokenSource

- CancellationTokenSource GetTokenSource(int id)
- `id` 任务id

获取任务id对应的CancellationTokenSource

```c#
var task = Runtime.Tasks.Run((token)=>
{
    //执行代码
});
var source = Runtime.Tasks.GetTokenSource(task.Id);
```





### Cancel - 取消

- void Cancel()

向所有任务发送取消信号

  ```c#
  Runtime.Tasks.Cancel();
  ```





