# EntryMethod - 入口方法特性



## ProjectEntryMethod - 项目入口方法特性

放置在项目入口方法



### IsUIMode - 是否为ui模式

- bool IsUIMode { get; set; }



## ScriptEntryMethod - 脚本入口方法特性

### IsUIMode - 是否为ui模式

- bool IsUIMode { get; set; }

### FileName - 脚本文件名

- string FileName { get; set; }